#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>


#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a)   (sizeof(a) / sizeof((a)[0]))
#endif


double bank_round(double num, int dec_places);
int ten_round(int number, int ten_places);

int main(int argc, char *argv[])
{
    printf("Hello World!\r\n");

#if 1
    double number[] =
    {
         0.2659799 ,   8.49758434  , -0.242500544  ,  0.177753508  ,   0.974313438  ,  0.0249560475   ,  0.0744817033  ,
         3.5764284 ,  -8.27353191  ,  0.582308352  , -0.57074821   ,   0.590003908  , -0.0151579808   , -0.04939273    ,
         1.3877968 ,  -5.60389471  ,  0.464067727  , -0.406638831  ,   0.361714244  , -0.00849434733  , -0.050879512   ,
         2.5314677 ,  -1.35992491  ,  0.181786045  , -0.164382562  ,   0.0704420209 , -0.00604748353  , -0.0190033186  ,
         0.3966336 ,   0.140390724 ,  0.0109187616 ,  0.0298451297 ,  -0.0422219709 , -0.00111219764  , -0.00283988635 ,
        -1.901039  , -82.3707275   ,  9.0515461    ,  9.33393002   , -10.0492582    , -0.498398781    , -0.822894692   ,
        -0.336193  ,   1.20948005  , -0.104367584  ,  0.126979768  ,   0.240529314  ,  0.000350224553 ,  0.990562081   ,

        1.964,
        1.9651,
        1.965,
        1.935,
        1.966,
        8.54321,
    };

    for (int i = 0; i < ARRAY_SIZE(number); i++)
    {
        double a = bank_round(number[i], 4);
        double b = (double)ten_round((int)(number[i] * 1000000), 2) / 1000000;

        printf("%02d: %10f -> %8.4f, %8.4f\r\n", i, number[i], a, b);
    }
#else
    int number[] =
    {
        49393,
        -5603895,
        8494,

        9905000, 9915000, 9925000, 9935000, 9945000, 9955000, 9965000, 9975000, 9985000, 9995000,

        // 1930000, 1931000, 1932000, 1933000, 1934000, 1935000, 1935100, 1935200, 1935300, 1935400, 1935500, 1935600, 1935700, 1935800, 1935900, 1936000, 1937000, 1938000, 1939000,
        // 1940000, 1941000, 1942000, 1943000, 1944000, 1945000, 1945100, 1945200, 1945300, 1945400, 1945500, 1945600, 1945700, 1945800, 1945900, 1946000, 1947000, 1948000, 1949000,
        // 1950000, 1951000, 1952000, 1953000, 1954000, 1955000, 1955100, 1955200, 1955300, 1955400, 1955500, 1955600, 1955700, 1955800, 1955900, 1956000, 1957000, 1958000, 1959000,
        // 1960000, 1961000, 1962000, 1963000, 1964000, 1965000, 1965100, 1965200, 1965300, 1965400, 1965500, 1965600, 1965700, 1965800, 1965900, 1966000, 1967000, 1968000, 1969000,
    };

    for (int i = 0; i < ARRAY_SIZE(number); i++)
    {
        printf("%d: %d -> %d\r\n", i, number[i], ten_round(number[i], 2));
    }
#endif

    system("pause");
    return 0;
}



/*
 * ten_places 整十位数
 */
int ten_round(int number, int ten_places)
{
    int neg = (number < 0) ? -1 : 1;
    number *= neg;

    int tens = 1;
    while (ten_places--) {
        tens *= 10;
    }

    int res = number / tens;
    int a = number * 10 / tens % 10;
    int b = number * 10 % tens;
    int c = (res & 0x01) ^ 0x01; /**< c = res % 2; */

    if (a > 5) {
        res++;
    } else if (a == 5) {
        if (b) {
            res++;
        } else {
            if (!c) {
                res++;
            }
        }
    }

    return neg * res * tens;
}




/*
 * dec_places 小数位数
 */
double bank_round(double number, int dec_places)
{
    double power = 1;

    while (dec_places--) {
        power *= 10;
    }

    double res_part = number * power;
    double int_part = (double)((int)res_part);
    double dec_part = res_part - int_part;

    if ((dec_part == 0.5) && ((int)int_part % 2 == 0)) {
        return (int_part / power);
    } else {
        return (res_part / power);
    }
}


